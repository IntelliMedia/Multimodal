﻿using System;

namespace Multimodal
{
    public enum SensorEnumType { OpenFace, OpenFaceMulti, Kinect, OpenSmile };
    public enum ActionEnumType { StartLogging, StopLogging };
    public enum LogEnumType { SingleUser, MultiUser };

    public class Command
    {
        public SensorEnumType SensorType { get; set; }
        public ActionEnumType ActionType { get; set; }
        public LogEnumType LogType { get; set; }
        public string UserID { get; set; }

        public Command(SensorEnumType sensorType, ActionEnumType actionType, LogEnumType logType, string id)
        {
            SensorType = sensorType;
            ActionType = actionType;
            LogType = logType;
            UserID = id;
        }
    }

    public class TrackingCheck
    {
        public SensorEnumType SensorType { get; set; }
        public bool IsTracking { get; set; }

        public TrackingCheck(SensorEnumType sensorType, bool isTracking)
        {
            SensorType = sensorType;
            IsTracking = isTracking;
        }
    }
}
