﻿using Fleck;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using MultimodalServer;
using System.Globalization;

namespace Multimodal
{
    public enum BodySendType
    {
        FirstTracked = 0, AllTracked, All
    }

    public class WebSocketServer
    {
        System.Timers.Timer tracking_timer = new System.Timers.Timer(1000 * 5);
        public bool IsKinectTracking;
        public bool IsOpenFaceTracking;
        public bool IsOpenFaceMultiTracking;
        public bool IsOpenSmileTracking;
        public bool IsStartLogging;

        private List<IWebSocketConnection> _sockets;
        private int _socketsCount;
        private SensorPool sensorPool = new SensorPool();

        public WebSocketServer()
        {
            tracking_timer.Enabled = true;
            tracking_timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTrackingTimerFired);

            IsKinectTracking = false;
            IsOpenFaceTracking = false;
            IsOpenFaceMultiTracking = false;
            IsOpenSmileTracking = false;

            IsStartLogging = false;
        }

        public void Start(ServerParameters serverParameters)
        {
            _sockets = new List<IWebSocketConnection>();

            var server = new Fleck.WebSocketServer(serverParameters.GetConnectionString());

            server.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    _sockets.Add(socket);
                    _socketsCount = _sockets.Count;
                    Console.WriteLine(string.Format("* -> Connected to {0}", socket.ConnectionInfo.ClientIpAddress));
                    // TODO: Need to get user ID from Future Worlds or client as a message
                };

                socket.OnError = (Exception e) =>
                {
                    Console.WriteLine("Server encountered an error:" + e);
                };

                socket.OnClose = async () =>
                {
                    _sockets.Remove(socket);
                    _socketsCount = _sockets.Count;
                    Console.WriteLine(string.Format("* -> Disconnected from {0}", socket.ConnectionInfo.ClientIpAddress));
                };

                socket.OnMessage = OnMessageReceived;
            });

            Console.ReadKey();
        }

        protected virtual async void OnMessageReceived(string message)
        {
            try
            {
                var obj = JsonConvert.DeserializeObject<Command>(message);
                var sensor = sensorPool.CreateOrFind(obj.SensorType, obj.LogType);
                sensor.CurrentUsername = obj.UserID;
                switch (obj.ActionType)
                {
                    case ActionEnumType.StartLogging:
                        await sensor.StartLoggingAsync(obj.LogType);
                        IsStartLogging = true;
                        break;
                    case ActionEnumType.StopLogging:
                        await sensor.StopLoggingAsync();
                        IsStartLogging = false;
                        break;
                    default:
                        throw new Exception($"Unrecognized Action {obj.ActionType}");
                }
                Console.WriteLine(message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(-1);
            }
        }

        public void SendData(string json)
        {
            for (int i = 0; i < _socketsCount; i++)
                _sockets[i].Send(json);
        }

        public static string Serialize(object target)
        {
            return JsonConvert.SerializeObject(target);
        }

        void OnTrackingTimerFired(object Sender, System.Timers.ElapsedEventArgs e)
        {
            if (!IsStartLogging)
                return;

            if (sensorPool.sensors.Count > 0)
            {
                foreach (var sensor in sensorPool.sensors)
                {
                    if (sensor.SensorType == SensorEnumType.OpenFace)
                    {
                        OpenFaceSensor new_sensor = (OpenFaceSensor)sensor;
                        IsOpenFaceTracking = new_sensor.GetDetectionSuccess();
                        SendData(Serialize(new TrackingCheck(SensorEnumType.OpenFace, IsOpenFaceTracking)));
                    }
                    else if (sensor.SensorType == SensorEnumType.OpenFaceMulti)
                    {
                        OpenFaceSensorMulti new_sensor = (OpenFaceSensorMulti)sensor;
                        IsOpenFaceMultiTracking = new_sensor.GetDetectionSuccess();
                        SendData(Serialize(new TrackingCheck(SensorEnumType.OpenFaceMulti, IsOpenFaceMultiTracking)));
                    }
                    else if (sensor.SensorType == SensorEnumType.Kinect)
                    {
                        KinectOneSensor new_sensor = (KinectOneSensor)sensor;
                        IsKinectTracking = new_sensor.GetDetectionSuccess();
                        SendData(Serialize(new TrackingCheck(SensorEnumType.Kinect, IsKinectTracking)));
                    }
                    else if (sensor.SensorType == SensorEnumType.OpenSmile)
                    {
                        OpenSmileSensor new_sensor = (OpenSmileSensor)sensor;
                        IsOpenSmileTracking = new_sensor.GetDetectionSuccess();
                        SendData(Serialize(new TrackingCheck(SensorEnumType.OpenSmile, IsOpenSmileTracking)));
                    }
                }
            }
        }
    }
}
