﻿using IntelliMediaCore.Common.DataModels;
using IntelliMediaCore.Common.Services;
using IntelliMediaCore.Common.Utilities;
using System;
using System.IO;
using System.Threading.Tasks;

namespace MultimodalServer
{
    class TraceLogController
    {
        static private LoggingService loggingSvc;

        public static bool IsLogging { get { return TraceLog.IsOpen; } }

        public static TraceStream Kinect { get { return TraceLog.Stream("Kinect"); }}
        public static TraceStream OpenFace { get { return TraceLog.Stream("OpenFace"); } }

        public static async Task StartLoggingAsync(string userId)
        {
            Console.WriteLine("Initialize Logging System");

            SessionState session = new SessionState
            {
                User = new User(),
            };

            SettingsService settingsService = new SettingsService(null);
            await settingsService.LoadAsync();
            settingsService.SetLocalSetting(LoggingService.OutputToLocalFile, true);

            loggingSvc = new LoggingService(null, settingsService);

            // Initialize default interaction trace logging (app events)    
            Console.WriteLine("Start logging");
            await loggingSvc.StartAsync(session);

            var openTimestamp = DateTime.Now.ToString("MM-dd-yyyy--HH-mm-ss");
            OpenLog("Kinect", userId, openTimestamp);
            OpenLog("OpenFace", userId, openTimestamp);

        }

        public static async Task StopLoggingAsync()
        {
            //if (IsLogging)
            if (TraceLogController.IsLogging)
            {
                Console.WriteLine("Stop logging");

                await Kinect.CloseAsync();

                await OpenFace.CloseAsync();

                // Close main interaction log for app
                await loggingSvc.StopAsync();
            }
        }

        private static void OpenLog(string streamName, string userId, string openTimestamp)
        {
            if (TraceLog.Stream(streamName).IsOpen)
            {
                return;
            }

            // Configure log file names using app name, user ID, and current Unix time
            var logFilename = string.Format("{0}_Sensors_{1}", userId, openTimestamp);
            if (string.Compare(streamName, TraceLog.DefaultStreamName) != 0)
            {
                logFilename += string.Format("-{0}", streamName);
            }
            string localFilename = String.Format("{0}.{1}", logFilename, SerializerCsv.Instance.FilenameExtension);

            var logDirectory = ChooseBaseDirectory();
            string traceLogDirectory = System.IO.Path.Combine(logDirectory != null ? logDirectory : ".", localFilename);
            TraceLog.Stream(streamName).Open(logFilename, new FileLogger(traceLogDirectory, SerializerCsv.Instance, true));

            DebugLog.Info("Opened '{0}' TraceLog", streamName);
            TraceLog.Write("System", TraceLog.Action.Opened, "SensorLog", "Type", streamName, "Filename", localFilename);
        }

        private static string ChooseBaseDirectory()
        {
            string baseDirectory = "./TraceData/";  // if no external drive is found, log to the working directory as a fallback
            string externalDriveName = "FutureWorldsDataCollection";    // Name of the external drive we want to log to


            // look through all drives in the system, searching for the named external drive
            foreach (DriveInfo d in DriveInfo.GetDrives())
            {
                try
                {
                    var driveName = d.VolumeLabel;
                    if (d.VolumeLabel.Contains(externalDriveName))
                    {
                        baseDirectory = d.Name + "/TraceData/";
                        break;
                    }
                }
                catch (System.IO.IOException)
                {
                    { } // d will return IOException if the drive is the DVD Drive
                }
            }

            return baseDirectory;
        }
    }
}
