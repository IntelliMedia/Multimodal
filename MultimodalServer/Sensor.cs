﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using Microsoft.Kinect;
using IntelliMediaCore.Common.Utilities;
using Newtonsoft.Json.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Accord.Video.FFMPEG;
using System.Drawing.Imaging;
using MultimodalServer;
using Multimodal;
using System.IO;
using System.Threading.Tasks;
using AudEERING.openSMILE;

namespace Multimodal
{
    public class Sensor
    {
        public string CurrentUsername { get; set; }
        public SensorEnumType SensorType { get; set; }
        public bool IsLogging = true;
        static void Initialize()
        {

        }

        static void Shutdown()
        {

        }

        public virtual Task StartLoggingAsync(LogEnumType mode)
        {
            if (TraceLogController.IsLogging)
            {
                return Task.CompletedTask;
            }
            else
            {
                return TraceLogController.StartLoggingAsync(CurrentUsername);
            }
        }

        public virtual Task StopLoggingAsync() => TraceLogController.StopLoggingAsync();
    }

    class OpenSmileWorker
    {
        private string myConfig = null;
        private string myOutput = null;
        private OpenSMILE mySmile = null;

        public OpenSmileWorker(OpenSMILE smile, string config, string output)
        {
            myConfig = config;
            myOutput = output;
            mySmile = smile;
        }
        public void Run()
        {
            try
            {
                Dictionary<string, string> options = new Dictionary<string, string>();
                options.Add("-O", myOutput);
                mySmile.Initialize(myConfig, options);
                mySmile.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public void Stop()
        {
            try
            {
                mySmile.Abort();
                mySmile.Dispose();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }

    public class OpenSmileSensor : Sensor
    {
        private OpenSMILE mySmile = null;
        private OpenSmileWorker mySmileWorker = null;
        private Thread runThread = null;
        private bool isInitialized = false;
        private string smileTraceFilename = null;
        private string externalDriveName = "FutureWorldsDataCollection";
        //private Process myProcess = null;
        //[DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        //private static extern int record(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);

        public OpenSmileSensor()
        {
            SensorType = SensorEnumType.OpenSmile;
            mySmile = new OpenSMILE();
            isInitialized = false;

            //myProcess = new Process();
        }
        public bool GetDetectionSuccess()
        {
            if (mySmile != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Initialize(string config, string output)
        {
            if (mySmile != null)
            {
                mySmile = null;
            }
            mySmile = new OpenSMILE();

            if (!String.IsNullOrEmpty(config) && !String.IsNullOrEmpty(output))
            {
                mySmileWorker = new OpenSmileWorker(mySmile, config, output);
                runThread = new Thread(mySmileWorker.Run);

                isInitialized = true;
            }
            else
            {
                throw new ArgumentNullException("Arguments cannot be null or empty");
            }
        }
        public void RunSmile()
        {
            if (isInitialized)
            {
                try
                {
                    runThread.Start();
                }
                catch
                {
                    throw;
                }
            }
            else
            {
                throw new Exception("Must Initialize first");
            }
        }

        public void StopSmile()
        {
            if (mySmileWorker != null)
            {
                try
                {
                    mySmileWorker.Stop();
                    runThread.Join();
                    isInitialized = false;
                    mySmileWorker = null;
                    mySmile = null;
                }
                catch
                {
                    throw;
                }
            }
        }

        public override async Task StartLoggingAsync(LogEnumType mode)
        {
            await base.StartLoggingAsync(mode);

            string conf = "liveProsodyAcf.conf";
            string logPath = GetLogDirectory();
            string filename = CurrentUsername + "_Sensors_" + DateTime.Now.ToString("MM-dd-yyyy--HH-mm-ss") +"-OpenSmile" + ".csv";
            smileTraceFilename = Path.Combine(logPath, filename);

            Initialize(conf, smileTraceFilename);
            RunSmile();
        }

        public override async Task StopLoggingAsync()
        {
            await base.StopLoggingAsync();

            StopSmile();
            smileTraceFilename = null;
        }

        private string GetLogDirectory()
        {
            string currentPath = Directory.GetCurrentDirectory();

            foreach (DriveInfo d in DriveInfo.GetDrives())
            {
                if (d.VolumeLabel.Contains(externalDriveName))
                {
                    currentPath = d.Name;
                    break;
                }
            }

            return Path.Combine(currentPath, @"TraceData\");
        }
    }

    public class KinectOneSensor : Sensor
    {
        private static KinectSensor _kinectSensor;
        private static Body[] _bodies;
        private static List<Body> _tracked = new List<Body>(6);
        private static MultiSourceFrameReader _reader;
        private static readonly BodySendType _sendType = BodySendType.AllTracked;
        private string externalDriveName = "FutureWorldsDataCollection";    // Name of the external drive we want to log to
        private string kinectVideoDirectory = "/KinectVideoData/";
        private bool bodyDetected = false;

        private static List<byte[]> _colorBitmaps;
        private static List<byte[]> _depthBitmaps;

        private static int _colorWidth = 1920;
        private static int _colorHeight = 1080;
        private static int _depthWidth = 512;
        private static int _depthHeight = 424;
        private static int _colorStride;
        private static int _depthStride;

        VideoFileWriter _colorVideoWriter;
        VideoFileWriter _depthVideoWriter;

        private static LogEnumType _logType;

        public KinectOneSensor()
        {
            SensorType = SensorEnumType.Kinect;
        }

        public override async Task StartLoggingAsync(LogEnumType mode)
        {
            await base.StartLoggingAsync(mode);
            _logType = mode;
            IsLogging = true;
            _colorVideoWriter = new VideoFileWriter();
            _depthVideoWriter = new VideoFileWriter();
            string KinectVideoDirectory = ChooseBaseDirectory();
            System.IO.Directory.CreateDirectory(KinectVideoDirectory);

            GetKinectData();
            string colorFilename = CurrentUsername + "_RGB_" + DateTime.Now.ToString("MM-dd-yyyy--HH-mm-ss") + ".mp4";
            string colorPath = System.IO.Path.Combine(KinectVideoDirectory != null ? KinectVideoDirectory : ".", colorFilename);
            _colorVideoWriter.Open(colorPath, _colorWidth, _colorHeight, 30, VideoCodec.MPEG4);

            string depthFilename = CurrentUsername + "_Depth_" + DateTime.Now.ToString("MM-dd-yyyy--HH-mm-ss") + ".mp4";
            string depthPath = System.IO.Path.Combine(KinectVideoDirectory != null ? KinectVideoDirectory : ".", depthFilename);
            _depthVideoWriter.Open(depthPath, _depthWidth, _depthHeight, 30, VideoCodec.MPEG4);
        }

        public override async Task StopLoggingAsync()
        {
            await base.StopLoggingAsync();
            IsLogging = false;
            StopGettingKinectData();

            _colorVideoWriter.Close();
            _colorVideoWriter.Dispose();

            _depthVideoWriter.Close();
            _depthVideoWriter.Dispose();
        }

        string ChooseBaseDirectory()
        {
            // if no external drive is found, log to the working directory as a fallback
            string baseDirectory = "." + kinectVideoDirectory;

            // look through all drives in the system, searching for the named external drive
            foreach (DriveInfo d in DriveInfo.GetDrives())
            {
                if (d.VolumeLabel.Contains(externalDriveName))
                {
                    baseDirectory = d.Name + kinectVideoDirectory;
                    break;
                }
            }

            return baseDirectory;
        }

        public void GetKinectData()
        {
            _kinectSensor = KinectSensor.GetDefault();

            if (_kinectSensor != null && IsLogging)
            {
                _kinectSensor.Open();
                _reader = _kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
                _reader.MultiSourceFrameArrived += OnMultiSourceFrameReady; // Will account for all streams of data (frames) coming from Kinect.
            }
        }

        public void StopGettingKinectData()
        {
            if (_kinectSensor != null)
            {
                _kinectSensor.Close();
            }

            _bodies = null;
            _kinectSensor = null;
            _tracked = new List<Body>(6);
            _reader = null;

            _colorBitmaps = new List<byte[]>();
            _depthBitmaps = new List<byte[]>();
        }

        private void OnMultiSourceFrameReady(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            // Obtaining multi-frame reference (sources are the parameters to OpenMultiSourceFrameReader function)
            var reference = e.FrameReference.AcquireFrame();

            // Opening Color frame(RGB data).
            using (var frame = reference.ColorFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    OnColorFrameReady(frame);
                }
            }

            // Opening Depth frame.
            using (var frame = reference.DepthFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    OnDepthFrameReady(frame);
                }
            }

            // Opening Body frame (skeletal data).
            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    OnBodyFrameReady(frame);
                }
            }

        }

        private void OnColorFrameReady(ColorFrame frame)
        {
            var frameDescription = _kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);
            _colorWidth = frameDescription.Width;
            _colorHeight = frameDescription.Height;
            uint frameSize = frameDescription.LengthInPixels * frameDescription.BytesPerPixel;
            var format = ColorImageFormat.Bgra;

            byte[] pixels = new byte[frameSize];

            frame.CopyConvertedFrameDataToArray(pixels, format);

            _colorStride = _colorWidth * PixelFormats.Bgr32.BitsPerPixel / 8;

            GCHandle pinnedArray = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            IntPtr pointer = pinnedArray.AddrOfPinnedObject();

            Bitmap newBitmap = new Bitmap(_colorWidth, _colorHeight, _colorStride, System.Drawing.Imaging.PixelFormat.Format32bppArgb, pointer);
            _colorVideoWriter.WriteVideoFrame(newBitmap);
            newBitmap.Dispose();
            pinnedArray.Free();
        }

        private void OnDepthFrameReady(DepthFrame frame)
        {
            _depthWidth = frame.FrameDescription.Width;
            _depthHeight = frame.FrameDescription.Height;

            ushort minDepth = frame.DepthMinReliableDistance;
            ushort maxDepth = frame.DepthMaxReliableDistance;

            ushort[] depthData = new ushort[_depthWidth * _depthHeight];
            byte[] pixelData = new byte[_depthWidth * _depthHeight * 3];

            frame.CopyFrameDataToArray(depthData);

            int colorIndex = 0;
            for (int depthIndex = 0; depthIndex < depthData.Length; ++depthIndex)
            {
                ushort depth = depthData[depthIndex];
                byte intensity = (byte)(depth >= minDepth && depth <= maxDepth ? depth : 0);

                pixelData[colorIndex++] = intensity; // Blue
                pixelData[colorIndex++] = intensity; // Green
                pixelData[colorIndex++] = intensity; // Red
            }

            _depthStride = _depthWidth * 3;

            GCHandle pinnedArray = GCHandle.Alloc(pixelData, GCHandleType.Pinned);
            IntPtr pointer = pinnedArray.AddrOfPinnedObject();

            Bitmap newBitmap = new Bitmap(_depthWidth, _depthHeight, _depthStride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, pointer);
            _depthVideoWriter.WriteVideoFrame(newBitmap);
            newBitmap.Dispose();
            pinnedArray.Free();
        }

        private void OnBodyFrameReady(BodyFrame frame)
        {
            var bodyCount = frame.BodyFrameSource.BodyCount;

            if (bodyCount == 0)
            {
                return;
            }

            if (_bodies == null)
            {
                _bodies = new Body[bodyCount];
                TraceLog.Write("System", TraceLog.Action.Sampled, "KinectOne", "Bodies", "Bodies Initialized");
            }

            // Getting the latest Kinect data.
            frame.GetAndRefreshBodyData(_bodies);

            if (_sendType == BodySendType.AllTracked)
                _tracked.Clear();


            // This List stores the indices and distances from the Kinect of each tracked skeleton in the Body array.
            List<KeyValuePair<int, float>> distanceList = new List<KeyValuePair<int, float>>();

            bodyDetected = false;

            // Get the distances for all tracked Bodies from the Kinect if they have a distance != 0.
            for (int i = 0; i < _bodies.Length; i++)
            {
                if (_bodies[i].IsTracked)
                {
                    bodyDetected = true;
                    float distance = _bodies[i].Joints[JointType.Head].Position.Z;
                    if (distance != 0)
                    {
                        distanceList.Add(new KeyValuePair<int, float>(i, distance));
                    }
                }
            }

            // Sorting the distances tracked in ascending order. This allows us to log the k closest users.
            distanceList.Sort((x, y) => (x.Value.CompareTo(y.Value)));

            // Only will log if at least one Body is being tracked.
            if (distanceList.Count > 0)
            {
                // There are 6 potential indices where a Body can be tracked, so we only log the indices where data is present
                // This also checks the logging type (multi- or single-user) and logs the appropriate # of users
                switch (_logType)
                {
                    case LogEnumType.SingleUser:
                        TraceLogController.Kinect.Write("System", TraceLog.Action.Sampled, "KinectOne", "Bodies", _bodies[distanceList[0].Key].Serialize(), "Tracked Body Index", distanceList[0].Key);
                        _tracked.Add(_bodies[distanceList[0].Key]);
                        break;
                    case LogEnumType.MultiUser:
                        int userIndex = 0;
                        // 3 users are being logged at max currently
                        while (userIndex < 3)
                        {
                            try
                            {
                                TraceLogController.Kinect.Write("System", TraceLog.Action.Sampled, "KinectOne", "Bodies", _bodies[distanceList[userIndex].Key].Serialize(), "Tracked Body Index", distanceList[userIndex].Key);
                                _tracked.Add(_bodies[distanceList[userIndex].Key]);
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                break;
                            }
                            userIndex++;
                        }
                        break;
                    default:
                        throw new Exception($"Unrecognized Log Type {_logType}");
                }
            }
        }

        public bool GetDetectionSuccess()
        {
            return bodyDetected;
        }

    }


    public class OpenFaceSensorMulti : Sensor
    {
        //// Open Camera and getdetection_success fns
        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "getdetection_success", CallingConvention = CallingConvention.Cdecl)]
        static extern int getdetection_success();

        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "main", CallingConvention = CallingConvention.StdCall)]
        static extern int main();

        //// Get Head pose [it could be replaced by one function by returning an array of doubles like in get XY]
        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "get_pose", CallingConvention = CallingConvention.Cdecl)]
        static extern int get_pose(out IntPtr pose_array);

        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "get_gaze0", CallingConvention = CallingConvention.StdCall)]
        public static extern int get_gaze0(out IntPtr gaze_array);

        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "get_gaze1", CallingConvention = CallingConvention.StdCall)]
        public static extern int get_gaze1(out IntPtr gaze_array);

        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "change_camera", CallingConvention = CallingConvention.StdCall)]
        static extern void change_camera();

        //// Get X Y Face Position (double array) | 2D landmark [x1;x2;...xn;y1;y2...yn] 
        //[DllImport("FaceLandmarkVid.dll", EntryPoint = "getXY", CallingConvention = CallingConvention.StdCall)]
        //public static extern int getXY(out IntPtr pArrayOfDouble2D);

        //// Get X Y Face Position (double array) | 3D landmark [x1;x2;...xn;y1;y2...yn;z1;z2...zn] 
        //[DllImport("FaceLandmarkVid.dll", EntryPoint = "getXYZ", CallingConvention = CallingConvention.StdCall)]
        //public static extern int getXYZ(out IntPtr pArrayOfDouble3D);

        // Get Action Unit Presence Array (double array) | Presence
        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "getAUPresence", CallingConvention = CallingConvention.StdCall)]
        public static extern int getAUPresence(out IntPtr pArrayOfDouble, int index);

        //// Get Action Unit Presence Name Array (string array) | Presence
        //[DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "getAUPresenceName", CallingConvention = CallingConvention.StdCall)]
        //public static extern int getAUPresenceName(byte[] buf);

        // Get Action Unit Intensity Array (double array) | Intensity
        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "getAUIntensity", CallingConvention = CallingConvention.StdCall)]
        public static extern int getAUIntensity(out IntPtr pArrayOfDouble, int index);

        //// Get Action Unit Presence Name Array (string array) | Presence
        //[DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "getAUIntensityName", CallingConvention = CallingConvention.StdCall)]
        //public static extern int getAUIntensityName(byte[] buf);

        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "get_face_ids", CallingConvention = CallingConvention.StdCall)]
        public static extern int get_face_ids(out IntPtr pArrayOfDouble);

        [DllImport("FaceLandmarkVidMulti.dll", EntryPoint = "quit", CallingConvention = CallingConvention.StdCall)]
        public static extern void quit();

        [DllImport("kernel32", SetLastError = true)]
        private static extern bool FreeLibrary(IntPtr hModule);

        public string[] ArrayOfStringAUPresenceName = new string[17];
        public string[] ArrayOfStringAUIntensityName = new string[17];
        public double[] ArrayOfDoubleAUPresence = new double[17];
        public double[] ArrayOfDoubleAUIntensity = new double[17];
        public float[] ArrayOfDouble2D = new float[136];
        public float[] ArrayOfDouble3D = new float[204];
        public float[] gaze_array_0 = new float[18];
        public float[] gaze_array_1 = new float[18];
        public float[] pose_array = new float[9];
        public int[] face_ids = new int[3];

        private static void MainWrapper()
        {
            main();
            Console.Out.WriteLine("Main Exit!");
        }

        public OpenFaceSensorMulti()
        {
            SensorType = SensorEnumType.OpenFaceMulti;
            // Thread to Open Camera
        }

        public bool GetDetectionSuccess()
        {
            if (getdetection_success() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override async Task StartLoggingAsync(LogEnumType mode)
        {
            // TODO: @nlhender implement multi-user logging based on this 'mode' parameter passed through the Command class.
            await base.StartLoggingAsync(mode);
            IsLogging = true;
            System.Threading.Thread newThread = new System.Threading.Thread(MainWrapper);
            // Start data stream
            newThread.Start();
            Thread.Sleep(5000);
            Thread thread = new Thread(new ThreadStart(getAUInfo));
            thread.Start();
        }

        public override async Task StopLoggingAsync()
        {
            await base.StopLoggingAsync();
            IsLogging = false;
            quit();
        }

        public void getAUInfo()
        {
            string[] AU_states = { "AU01", "AU02", "AU04", "AU05", "AU06", "AU07", "AU09",
            "AU10", "AU12", "AU14", "AU15", "AU17", "AU20", "AU23", "AU25", "AU26", "AU28"};
            int[] presence_indeces = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

            while (IsLogging)
            {
                // Create JSON object to hold all logged data
                JObject output_json = new JObject();

                // Get multi-user gaze data
                IntPtr p_gaze_array_0 = IntPtr.Zero;
                IntPtr p_gaze_array_1 = IntPtr.Zero;

                get_gaze0(out p_gaze_array_0);
                get_gaze1(out p_gaze_array_1);

                Marshal.Copy(p_gaze_array_0, gaze_array_0, 0, 9);
                Marshal.Copy(p_gaze_array_1, gaze_array_1, 0, 9);

                // Get multi-user head pose data
                IntPtr p_pose_array = IntPtr.Zero;
                get_pose(out p_pose_array);
                Marshal.Copy(p_pose_array, pose_array, 0, 9);

                // Get AU names and info for all 3 users
                for (int i = 0; i < 3; i++)
                {
                    IntPtr pAUIntensityDoubleArray = IntPtr.Zero;
                    getAUIntensity(out pAUIntensityDoubleArray, i);
                    Marshal.Copy(pAUIntensityDoubleArray, ArrayOfDoubleAUIntensity, 0, 17);

                    IntPtr pAUPresenceDoubleArray = IntPtr.Zero;
                    getAUPresence(out pAUPresenceDoubleArray, i);
                    Marshal.Copy(pAUPresenceDoubleArray, ArrayOfDoubleAUPresence, 0, 17);

                    // get names!
                    //byte[] bufPresence = new byte[20 * 5];
                    //getAUPresenceName(bufPresence);
                    //byte[] bufIntensity = new byte[20 * 5];
                    //getAUIntensityName(bufIntensity);

                    //for (int j = 0; j < 17; j++)
                    //{
                    //    ArrayOfStringAUPresenceName[j] = Encoding.UTF8.GetString(bufPresence.Skip(j * 4).Take(4).ToArray());
                    //    ArrayOfStringAUIntensityName[j] = Encoding.UTF8.GetString(bufIntensity.Skip(j * 4).Take(4).ToArray());
                    //}
                    Array.Sort(ArrayOfStringAUPresenceName, presence_indeces);
                    for (int j = 0; j < 17; j++)
                    {
                        //string AU_presence_name = ArrayOfStringAUPresenceName[j];
                        string AU_presence_name = AU_states[j];
                        string AU_presence_data = ArrayOfDoubleAUPresence[presence_indeces[j]].ToString();
                        // string AU_intensity_name = ArrayOfStringAUIntensityName[j];
                        string AU_intensity_name = AU_states[j];
                        string AU_intensity_data;
                        if (AU_presence_name != "AU28" && AU_presence_name != "AU45")
                        {
                            //AU_intensity_data = ArrayOfDoubleAUIntensity[Array.IndexOf(ArrayOfStringAUIntensityName, AU_intensity_name)].ToString();
                            AU_intensity_data = ArrayOfDoubleAUIntensity[j].ToString();
                        }
                        else
                        {
                            AU_intensity_data = "0";
                        }
                        output_json.Add(new JProperty(String.Concat(AU_presence_name.Substring(0, 4), "_", (i + 1)), new JObject(
                            new JProperty("Presence", AU_presence_data),
                            new JProperty("Intensity", AU_intensity_data))));
                    }
                }


                // Write Head Pose Data to JSON file
                output_json.Add(new JProperty("HeadPose_XYZ_1", new JArray()));
                output_json.Add(new JProperty("HeadPose_XYZ_2", new JArray()));
                output_json.Add(new JProperty("HeadPose_XYZ_3", new JArray()));
                JArray json_head_pose_1 = (JArray)output_json["HeadPose_XYZ_1"];
                JArray json_head_pose_2 = (JArray)output_json["HeadPose_XYZ_2"];
                JArray json_head_pose_3 = (JArray)output_json["HeadPose_XYZ_3"];
                json_head_pose_1.Add(pose_array[0]);
                json_head_pose_1.Add(pose_array[1]);
                json_head_pose_1.Add(pose_array[2]);
                json_head_pose_2.Add(pose_array[3]);
                json_head_pose_2.Add(pose_array[4]);
                json_head_pose_2.Add(pose_array[5]);
                json_head_pose_3.Add(pose_array[6]);
                json_head_pose_3.Add(pose_array[7]);
                json_head_pose_3.Add(pose_array[8]);

                //// Write eye gaze data to JSON file
                output_json.Add(new JProperty("EyeGaze_1", new JArray()));
                output_json.Add(new JProperty("EyeGaze_2", new JArray()));
                output_json.Add(new JProperty("EyeGaze_3", new JArray()));
                JArray json_eye_gaze_1 = (JArray)output_json["EyeGaze_1"];
                JArray json_eye_gaze_2 = (JArray)output_json["EyeGaze_2"];
                JArray json_eye_gaze_3 = (JArray)output_json["EyeGaze_3"];
                json_eye_gaze_1.Add(gaze_array_0[0]);
                json_eye_gaze_1.Add(gaze_array_0[1]);
                json_eye_gaze_1.Add(gaze_array_0[2]);
                json_eye_gaze_1.Add(gaze_array_1[0]);
                json_eye_gaze_1.Add(gaze_array_1[1]);
                json_eye_gaze_1.Add(gaze_array_1[2]);
                json_eye_gaze_2.Add(gaze_array_0[3]);
                json_eye_gaze_2.Add(gaze_array_0[4]);
                json_eye_gaze_2.Add(gaze_array_0[5]);
                json_eye_gaze_2.Add(gaze_array_1[3]);
                json_eye_gaze_2.Add(gaze_array_1[4]);
                json_eye_gaze_2.Add(gaze_array_1[5]);
                json_eye_gaze_3.Add(gaze_array_0[6]);
                json_eye_gaze_3.Add(gaze_array_0[7]);
                json_eye_gaze_3.Add(gaze_array_0[8]);
                json_eye_gaze_3.Add(gaze_array_1[6]);
                json_eye_gaze_3.Add(gaze_array_1[7]);
                json_eye_gaze_3.Add(gaze_array_1[8]);

                // Write Face IDs to JSON file
                IntPtr p_face_ids = IntPtr.Zero;
                get_face_ids(out p_face_ids);
                Marshal.Copy(p_face_ids, face_ids, 0, 3);
                output_json.Add(new JProperty("Face_IDs", new JArray()));
                JArray json_face_ids = (JArray)output_json["Face_IDs"];
                json_face_ids.Add(face_ids[0]);
                json_face_ids.Add(face_ids[1]);
                json_face_ids.Add(face_ids[2]);

                //IntPtr p2DDoubleArray = IntPtr.Zero;
                //getXY(out p2DDoubleArray);
                //Marshal.Copy(p2DDoubleArray, ArrayOfDouble2D, 0, 136);

                //// Write 2D Landmark coordinates to JSON output
                //output_json.Add(new JProperty("Landmarks2D_X", new JArray()));
                //JArray json_2D_landmarks = (JArray)output_json["Landmarks2D_X"];
                //for(int i = 0; i < 136/2; i++)
                //{
                //    json_2D_landmarks.Add(ArrayOfDouble2D[i]);
                //}
                //output_json.Add(new JProperty("Landmarks2D_Y", new JArray()));
                //json_2D_landmarks = (JArray)output_json["Landmarks2D_Y"];
                //for (int i = 136/2; i < 136; i++)
                //{
                //    json_2D_landmarks.Add(ArrayOfDouble2D[i]);
                //}
                //IntPtr p3DDoubleArray = IntPtr.Zero;
                //getXYZ(out p3DDoubleArray);
                //Marshal.Copy(p3DDoubleArray, ArrayOfDouble3D, 0, 204);

                if (IsLogging)
                {
                    TraceLogController.OpenFace.Write("System", TraceLog.Action.Sampled, "OpenFace", "OpenFaceFeatures", output_json.ToString());
                }
                Thread.Sleep(33);
            }
        }
    }

    public class OpenFaceSensor : Sensor
    {
        // Open Camera and getdetection_success fns
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "getdetection_success", CallingConvention = CallingConvention.Cdecl)]
        static extern int getdetection_success();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "main", CallingConvention = CallingConvention.StdCall)]
        static extern int main();

        // Get Head pose [it could be replaced by one function by returning an array of doubles like in get XY]
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_pose1", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_pose1();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_pose2", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_pose2();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_pose3", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_pose3();

        // Get eyes gaze [it could be replaced by one function by returning an array of doubles like in get XY]
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_gaze1", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_gaze1();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_gaze2", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_gaze2();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_gaze3", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_gaze3();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_gaze4", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_gaze4();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_gaze5", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_gaze5();
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "get_gaze6", CallingConvention = CallingConvention.Cdecl)]
        static extern float get_gaze6();

        [DllImport("FaceLandmarkVid.dll", EntryPoint = "change_camera", CallingConvention = CallingConvention.StdCall)]
        static extern void change_camera();

        // Get X Y Face Position (double array) | 2D landmark [x1;x2;...xn;y1;y2...yn] 
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "getXY", CallingConvention = CallingConvention.StdCall)]
        public static extern int getXY(out IntPtr pArrayOfDouble2D);

        // Get X Y Face Position (double array) | 3D landmark [x1;x2;...xn;y1;y2...yn;z1;z2...zn] 
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "getXYZ", CallingConvention = CallingConvention.StdCall)]
        public static extern int getXYZ(out IntPtr pArrayOfDouble3D);

        // Get Action Unit Presence Array (double array) | Presence
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "getAUPresence", CallingConvention = CallingConvention.StdCall)]
        public static extern int getAUPresence(out IntPtr pArrayOfDouble);

        // Get Action Unit Presence Name Array (string array) | Presence
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "getAUPresenceName", CallingConvention = CallingConvention.StdCall)]
        public static extern int getAUPresenceName(byte[] buf);

        // Get Action Unit Intensity Array (double array) | Intensity
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "getAUIntensity", CallingConvention = CallingConvention.StdCall)]
        public static extern int getAUIntensity(out IntPtr pArrayOfDouble);

        // Get Action Unit Presence Name Array (string array) | Presence
        [DllImport("FaceLandmarkVid.dll", EntryPoint = "getAUIntensityName", CallingConvention = CallingConvention.StdCall)]
        public static extern int getAUIntensityName(byte[] buf);

        [DllImport("FaceLandmarkVid.dll", EntryPoint = "quit", CallingConvention = CallingConvention.StdCall)]
        public static extern void quit();

        [DllImport("kernel32", SetLastError = true)]
        private static extern bool FreeLibrary(IntPtr hModule);

        public string[] ArrayOfStringAUPresenceName = new string[18];
        public string[] ArrayOfStringAUIntensityName = new string[18];
        public double[] ArrayOfDoubleAUPresence = new double[18];
        public double[] ArrayOfDoubleAUIntensity = new double[18];
        public float[] ArrayOfDouble2D = new float[136];
        public float[] ArrayOfDouble3D = new float[204];

        private static void MainWrapper()
        {
            main();
            Console.Out.WriteLine("Main Exit!");
        }

        public OpenFaceSensor()
        {
            SensorType = SensorEnumType.OpenFace;
            // Thread to Open Camera
        }

        public bool GetDetectionSuccess()
        {
            if (getdetection_success() == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override async Task StartLoggingAsync(LogEnumType mode)
        {
            await base .StartLoggingAsync(mode);
            IsLogging = true;
            System.Threading.Thread newThread = new System.Threading.Thread(MainWrapper);
            // Start data stream
            newThread.Start();
            Thread.Sleep(5000);
            Thread thread = new Thread(new ThreadStart(getAUInfo));
            thread.Start();
        }

        public override async Task StopLoggingAsync()
        {
            await base .StopLoggingAsync();
            IsLogging = false;
            quit();
        }

        public void getAUInfo()
        {
            string[] AU_states = { "AU01", "AU02", "AU04", "AU05", "AU06", "AU07", "AU09",
            "AU10", "AU12", "AU14", "AU15", "AU17", "AU20", "AU23", "AU25", "AU26", "AU28", "AU45"};
            int[] presence_indeces = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };

            while (IsLogging)
            {
                JObject output_json = new JObject();
                IntPtr pAUPresenceDoubleArray = IntPtr.Zero;
                getAUPresence(out pAUPresenceDoubleArray);
                Marshal.Copy(pAUPresenceDoubleArray, ArrayOfDoubleAUPresence, 0, 18);

                IntPtr pAUIntensityDoubleArray = IntPtr.Zero;
                getAUIntensity(out pAUIntensityDoubleArray);
                Marshal.Copy(pAUIntensityDoubleArray, ArrayOfDoubleAUIntensity, 0, 18);

                // get names!
                byte[] bufPresence = new byte[20 * 5];
                getAUPresenceName(bufPresence);
                byte[] bufIntensity = new byte[20 * 5];
                getAUIntensityName(bufIntensity);

                for (int i = 0; i < 18; i++)
                {
                    ArrayOfStringAUPresenceName[i] = Encoding.UTF8.GetString(bufPresence.Skip(i * 4).Take(4).ToArray());
                    ArrayOfStringAUIntensityName[i] = Encoding.UTF8.GetString(bufIntensity.Skip(i * 4).Take(4).ToArray());
                }

                // Write Head Pose Data to JSON file
                float head_pose_x = get_pose1();
                float head_pose_y = get_pose2();
                float head_pose_z = get_pose3();
                output_json.Add(new JProperty("HeadPose_XYZ", new JArray()));
                JArray json_head_pose = (JArray)output_json["HeadPose_XYZ"];
                json_head_pose.Add(head_pose_x);
                json_head_pose.Add(head_pose_y);
                json_head_pose.Add(head_pose_z);

                // Write eye gaze data to JSON file
                float gaze_1 = get_gaze1();
                float gaze_2 = get_gaze2();
                float gaze_3 = get_gaze3();
                float gaze_4 = get_gaze4();
                float gaze_5 = get_gaze5();
                float gaze_6 = get_gaze6();
                output_json.Add(new JProperty("EyeGaze", new JArray()));
                JArray json_eye_gaze = (JArray)output_json["EyeGaze"];
                json_eye_gaze.Add(gaze_1);
                json_eye_gaze.Add(gaze_2);
                json_eye_gaze.Add(gaze_3);
                json_eye_gaze.Add(gaze_4);
                json_eye_gaze.Add(gaze_5);
                json_eye_gaze.Add(gaze_6);

                IntPtr p2DDoubleArray = IntPtr.Zero;
                getXY(out p2DDoubleArray);
                Marshal.Copy(p2DDoubleArray, ArrayOfDouble2D, 0, 136);

                // Write 2D Landmark coordinates to JSON output
                output_json.Add(new JProperty("Landmarks2D_X", new JArray()));
                JArray json_2D_landmarks = (JArray)output_json["Landmarks2D_X"];
                for (int i = 0; i < 136 / 2; i++)
                {
                    json_2D_landmarks.Add(ArrayOfDouble2D[i]);
                }
                output_json.Add(new JProperty("Landmarks2D_Y", new JArray()));
                json_2D_landmarks = (JArray)output_json["Landmarks2D_Y"];
                for (int i = 136 / 2; i < 136; i++)
                {
                    json_2D_landmarks.Add(ArrayOfDouble2D[i]);
                }
                IntPtr p3DDoubleArray = IntPtr.Zero;
                getXYZ(out p3DDoubleArray);
                Marshal.Copy(p3DDoubleArray, ArrayOfDouble3D, 0, 204);

                Array.Sort(ArrayOfStringAUPresenceName, presence_indeces);
                for (int i = 0; i < 18; i++)
                {
                    string AU_presence_name = ArrayOfStringAUPresenceName[i];
                    string AU_presence_data = ArrayOfDoubleAUPresence[presence_indeces[i]].ToString();
                    string AU_intensity_name = ArrayOfStringAUPresenceName[i];
                    string AU_intensity_data;
                    if (AU_presence_name != "AU28")
                    {
                        AU_intensity_data = ArrayOfDoubleAUIntensity[Array.IndexOf(ArrayOfStringAUIntensityName, AU_presence_name)].ToString();
                    }
                    else
                    {
                        AU_intensity_data = "0";
                    }
                    output_json.Add(new JProperty(AU_presence_name.Substring(0, 4), new JObject(
                        new JProperty("Presence", AU_presence_data),
                        new JProperty("Intensity", AU_intensity_data))));
                }
                if (IsLogging)
                {
                    TraceLogController.OpenFace.Write("System", TraceLog.Action.Sampled, "OpenFace", "OpenFaceFeatures", output_json.ToString());
                }
                Thread.Sleep(33);
            }
        }

    }
}