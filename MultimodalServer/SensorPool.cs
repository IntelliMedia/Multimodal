﻿using Multimodal;
using System;
using System.Collections.Generic;
using System.Linq;

class SensorPool
{
    public List<Sensor> sensors = new List<Sensor>();

    public Sensor CreateOrFind(SensorEnumType sensorType, LogEnumType logType)
    {
        Sensor sensor = null;
        switch (sensorType)
        {
            case SensorEnumType.Kinect:
                sensor = sensors.FirstOrDefault(s => s is KinectOneSensor);
                if(sensor == null)
                {
                    sensor = new KinectOneSensor();
                    sensors.Add(sensor);
                }
                break;
            case SensorEnumType.OpenFace:
                sensor = sensors.FirstOrDefault(s => s is OpenFaceSensor);
                if (sensor == null)
                {
                    sensor = new OpenFaceSensor();
                    sensors.Add(sensor);
                }
                break;
            case SensorEnumType.OpenFaceMulti:
                sensor = sensors.FirstOrDefault(s => s is OpenFaceSensorMulti);
                if (sensor == null)
                {
                    sensor = new OpenFaceSensorMulti();
                    sensors.Add(sensor);
                }
                break;
            case SensorEnumType.OpenSmile:
                sensor = sensors.FirstOrDefault(s => s is OpenSmileSensor);
                if (sensor == null)
                {
                    sensor = new OpenSmileSensor();
                    sensors.Add(sensor);
                }
                break;
            default:
                throw new Exception("Unknown Sensor Type " + sensorType.ToString());
        }

        return sensor;
    }
}