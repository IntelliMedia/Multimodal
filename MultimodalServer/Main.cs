﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;

namespace Multimodal
{
    class Program
    {
        private static WebSocketServer _server;

        public static void Main(string[] args)
        {
            try
            {
                var parameters = new ServerParameters(args);
                _server = new WebSocketServer();

                // TODO: Print console message indicating what sensors are available at this point
                _server.Start(parameters);
                // PrintServerHeader();
            }

            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public static void PrintServerHeader()
        {
            Console.WriteLine("************************************");
            Console.WriteLine(string.Format("* Multimodal Server Ready! *"));
            Console.WriteLine("************************************");

            Console.WriteLine("************************************");
            Console.WriteLine("");
        }
    }
}
